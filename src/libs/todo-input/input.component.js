import PropTypes from 'prop-types';

const variantDefault = (inputProps) => {
  return (
    <input
      className='mr-3 rounded-lg border-2 border-slate-300 px-3 py-1 text-indigo-500 outline-indigo-500'
      type='text'
      {...inputProps}
    />
  );
};

const variantDropDown = (options, inputProps) => {
  const onRenderOption = () => {
    if (options && typeof options === 'object') {
      return options.map((item) => {
        return (
          <option key={item.value} value={item.value}>
            {item.text}
          </option>
        );
      });
    }
  };
  return (
    <select
      className='cursor-pointer rounded-lg border-2 border-slate-300 px-3 py-1 text-indigo-500 outline-indigo-500'
      {...inputProps}
    >
      {onRenderOption()}
    </select>
  );
};

TodoInputComponent.propTypes = {
  type: PropTypes.oneOf(['default']),
  options: PropTypes.exact([
    {
      value: PropTypes.string,
      text: PropTypes.string,
    },
  ]),
};

TodoInputComponent.defaultProps = {
  type: 'default',
  options: [
    {
      value: '',
      text: 'All',
    },
  ],
};

function TodoInputComponent(props) {
  let {type, options, ...inputProps} = props;
  let res;

  const onRender = () => {
    switch (type) {
      case 'default':
        res = variantDefault(inputProps);
        break;

      case 'drop-down':
        res = variantDropDown(options, inputProps);
        break;

      default:
        break;
    }
    return res;
  };

  return onRender();
}

export default TodoInputComponent;
