import {Link} from 'react-router-dom';
import {ASSETS_ENUM} from '../../utils';

function NotFound() {
  return (
    <div className='h-screen bg-slate-100 px-5'>
      <div className='flex h-full flex-col items-center justify-center'>
        <div className='text-center'>
          <h2 className='mb-3 text-5xl font-semibold text-[#6969F9] md:text-5xl lg:text-5xl'>
            OOPS!
          </h2>
          <p className='text-base font-medium text-[#7A320A] md:text-2xl lg:text-xl'>
            The Page You Requested Could Not Be Found.
          </p>
        </div>
        <img
          className='my-6 w-5/6 rounded-full shadow-xl transition-all duration-300 hover:scale-x-110 hover:scale-y-90 hover:hue-rotate-90 hover:filter md:my-10 md:w-3/5 lg:my-8 lg:w-2/5 xl:w-1/4'
          src={ASSETS_ENUM.NOT_FOUND_IMG}
          alt='404'
        />
        <p className='text-center text-base font-medium leading-5 text-[#7A320A] md:text-2xl md:leading-10 lg:text-xl'>
          Your cat is on the lab now! Click{' '}
          <Link
            to='/home'
            className='text-xl text-[#6969F9] hover:underline md:text-3xl lg:text-2xl'
          >
            HERE
          </Link>{' '}
          to move him out &#128514;
        </p>
      </div>
    </div>
  );
}

export default NotFound;
