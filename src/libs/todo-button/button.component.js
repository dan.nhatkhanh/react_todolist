import PropTypes from 'prop-types';

const transition = 'transition duration-300';

const bgPrimary = 'bg-indigo-500 hover:bg-white';
const textPrimary = 'text-lg font-medium text-white hover:text-indigo-500';
const borderPrimary = 'border-2 border-transparent focus:border-indigo-300';
const shadowPrimary = 'hover:shadow-md hover:shadow-indigo-500';

const bgSmooth = 'bg-indigo-100 hover:bg-white';
const textSmooth = 'text-lg font-medium text-indigo-500';
const borderSmooth = 'border-2 border-transparent focus:border-indigo-300';
const shadowSmooth = 'hover:shadow-md hover:shadow-indigo-500';

const bgGhost = 'bg-white hover:bg-indigo-500';
const textGhost = 'text-lg font-medium text-indigo-500 hover:text-white';
const borderGhost =
  'border-[1px] border-indigo-500 hover:border-transparent focus:border-indigo-300';
const shadowGhost = 'hover:shadow-md hover:shadow-indigo-300';

const bgRaised = 'bg-white hover:bg-indigo-500';
const textRaised = 'text-lg font-medium text-indigo-500 hover:text-white';
const borderRaised = 'border-2 border-transparent focus:border-indigo-300';
const shadowRaised = 'shadow-md shadow-indigo-500';

const shapeDefault = 'px-6 py-1 rounded-md';
const shapeCircle = 'w-14 h-14 rounded-full';
const shapeSquare = 'w-14 h-14 rounded-lg';
const shapeAction = 'px-5 py-2 rounded-full';

const iconStyle = 'text-xl';

const handleChangeShape = (shape) => {
  switch (shape) {
    case 'default':
      return shapeDefault;
    case 'circle':
      return shapeCircle;
    case 'square':
      return shapeSquare;
    case 'action':
      return shapeAction;

    default:
      return shapeDefault;
  }
};

const handleIconContentDistance = (icon, content, shape) => {
  return icon && content && shape !== 'circle' && shape !== 'square'
    ? 'mr-3'
    : '';
};

const handleHiddenContent = (shape, content) => {
  return shape === 'circle' || shape === 'square' ? '' : content;
};

const handleHiddenIcon = (content, icon, shape) => {
  return (
    shape !== 'default' && (
      <i
        className={`${icon} ${iconStyle} ${handleIconContentDistance(
          icon,
          content,
          shape
        )}`}
      ></i>
    )
  );
};

const variantDefault = (content, icon, shape, className, buttonProps) => {
  return (
    <button
      className={`${className} ${bgPrimary} ${textPrimary} ${borderPrimary} ${handleChangeShape(
        shape
      )} ${shadowPrimary} ${transition}`}
      {...buttonProps}
    >
      {handleHiddenIcon(content, icon, shape)}{' '}
      {handleHiddenContent(shape, content)}
    </button>
  );
};

const variantSmooth = (content, icon, shape, className, buttonProps) => {
  return (
    <>
      <button
        className={`${className} ${bgSmooth} ${textSmooth} ${borderSmooth} ${handleChangeShape(
          shape
        )}  ${shadowSmooth} ${transition}`}
        {...buttonProps}
      >
        {handleHiddenIcon(content, icon, shape)}{' '}
        {handleHiddenContent(shape, content)}
      </button>
    </>
  );
};

const variantGhost = (content, icon, shape, className, buttonProps) => {
  return (
    <>
      <button
        className={`${className} ${bgGhost} ${textGhost} ${borderGhost} ${handleChangeShape(
          shape
        )}  ${shadowGhost} ${transition}`}
        {...buttonProps}
      >
        {handleHiddenIcon(content, icon, shape)}{' '}
        {handleHiddenContent(shape, content)}
      </button>
    </>
  );
};

const variantRaised = (content, icon, shape, className, buttonProps) => {
  return (
    <>
      <button
        className={`${className} ${bgRaised} ${textRaised} ${borderRaised} ${handleChangeShape(
          shape
        )}  ${shadowRaised} ${transition}`}
        {...buttonProps}
      >
        {handleHiddenIcon(content, icon, shape)}{' '}
        {handleHiddenContent(shape, content)}
      </button>
    </>
  );
};

const variantBtnTable = (icon, className, buttonProps) => {
  return (
    <>
      <button
        className={`${className} text-base font-normal hover:text-indigo-400`}
        {...buttonProps}
      >
        <i className={icon}></i>
      </button>
    </>
  );
};

TodoButtonComponent.propTypes = {
  type: PropTypes.oneOf(['default', 'smooth', 'ghost', 'raised', 'table']),
  content: PropTypes.string,
  icon: PropTypes.string,
  shape: PropTypes.oneOf(['default', 'circle', 'square', 'action']),
};

TodoButtonComponent.defaultProps = {
  type: 'default',
  content: 'button',
  icon: 'fa-solid fa-plus',
  shape: 'default',
};

function TodoButtonComponent(props) {
  let {type, content, icon, shape, className, ...buttonProps} = props;
  let res;
  const onRender = () => {
    switch (type) {
      case 'default':
        res = variantDefault(content, icon, shape, className, buttonProps);
        break;

      case 'smooth':
        res = variantSmooth(content, icon, shape, className, buttonProps);
        break;

      case 'ghost':
        res = variantGhost(content, icon, shape, className, buttonProps);
        break;

      case 'raised':
        res = variantRaised(content, icon, shape, className, buttonProps);
        break;

      case 'table':
        res = variantBtnTable(icon, className, buttonProps);
        break;

      default:
        break;
    }

    return res;
  };

  return <div>{onRender()}</div>;
}

export default TodoButtonComponent;
