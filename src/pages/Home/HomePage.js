import TodoHeader from '../../components/TodoHeader/TodoHeader';

function HomePage() {
  console.log('Homepage neeeeee');
  return (
    <div className='h-screen bg-slate-100 px-5'>
      <div className='container mx-auto py-5'>
        <TodoHeader />
      </div>
    </div>
  );
}

export default HomePage;
