import {TodoButton} from '../../libs';

function TodoInput() {
  return (
    <div className='flex flex-wrap'>
      <TodoInput placeholder='Add task' />
      <TodoButton content='Add' />
    </div>
  );
}

export default TodoInput;
